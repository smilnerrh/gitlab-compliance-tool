# This file is part of gitlab-compliance-tool.
#
# Copyright (C) 2023 Steve Milner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import logging

import pytest

from gct.__main__ import GitLabComplianceTool


def test_init():
    """
    Verify object init stores the proper data for use.
    """
    logger = logging.getLogger('test')
    g = GitLabComplianceTool(logger)

    assert g.logger == logger
    assert g.group_id is None
    assert g.private_token is None
    assert g._conn is not None

    g = GitLabComplianceTool(logger, 'secret', '1234')
    assert g.logger == logger
    assert g.group_id is '1234'
    assert g.private_token is 'secret'
    assert g._conn is not None


def test__validate():
    """
    Ensure validate errors when we are missing critical information.
    """
    logger = logging.getLogger('test')
    g = GitLabComplianceTool(logger)

    with pytest.raises(SystemExit):
        g._validate()

    g = GitLabComplianceTool(logger, 'secret', '1234')
    assert g._validate() is None


def test_check_without_data():
    """
    Verify check fails to run when we are missing critical information.
    """
    logger = logging.getLogger('test')
    g = GitLabComplianceTool(logger)

    with pytest.raises(SystemExit):
        g.check()


def test__parse_private_allow_list():
    """
    Check the parsing for allow list input.
    """
    expected = ['one', 'two', 'three']

    logger = logging.getLogger('test')
    g = GitLabComplianceTool(logger)
    g._parse_private_allow_list('one,two,three')
    assert g._private_allow_list == expected

    g._parse_private_allow_list('one, two, three')
    assert g._private_allow_list == expected

    g._parse_private_allow_list('   one,two, three ')
    assert g._private_allow_list == expected