# This file is part of gitlab-compliance-tool.
#
# Copyright (C) 2023 Steve Milner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import subprocess

import pytest


def _run(cli_args: list) -> subprocess.Popen:
    """
    Runs the CLI from source inside of poetry.
    """
    args = ["poetry", "run", "python3", "gct/__main__.py"]
    args = args + cli_args
    cmd = subprocess.Popen(
        args,
        env={"PYTHONPATH": os.getcwd()},
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    # Wait for up to 20 seconds for the command to finish
    # before killing the process
    cmd.wait(20.0)
    return cmd


def test_help():
    """
    Ensure --help works.
    """
    cmd = _run(["--help"])
    assert cmd.returncode == 0
    assert len(cmd.stderr.readlines()) == 0
    assert len(cmd.stdout.readlines()) > 1


def test_bare_cli():
    """
    Ensure running with no options notifies the user of missing input.
    """
    cmd = _run([])
    assert cmd.returncode == 1
    assert len(cmd.stderr.readlines()) == 1
    assert len(cmd.stdout.readlines()) == 0
