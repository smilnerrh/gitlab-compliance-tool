# This file is part of gitlab-compliance-tool.
#
# Copyright (C) 2023 Steve Milner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import logging
import os

from typing import Optional

import gitlab
import gitlab.v4.objects
import requests

from gct import __version__


class GitLabComplianceTool:
    """
    Encapsulation of GitLab compliance tool.
    """

    def __init__(
        self,
        logger: logging.Logger,
        private_token: Optional[str] = None,
        group_id: Optional[str] = None,
        private_allow_list: Optional[str] = None,
    ):
        """
        Initializes a new instance.

        :param logger: The logger instance to use for logging
        :param private_token: Optional token to use with GitLab
        :param group_id: Starting group. Must be set here or in env var.
        :param private_allow_list: Optional list of allowed private projects
        """
        self._prj_cnt = 0
        self._grp_cnt = 0
        self.logger = logger
        self.logger.debug(
            f"Initializing a new instance of {self.__class__.__name__}"
        )

        if private_token is None:
            private_token = os.environ.get("GCT_PRIVATE_TOKEN", None)
        self.private_token = private_token
        self.logger.debug(f"private_token={self.private_token}")

        if group_id is None:
            group_id = os.environ.get("GCT_GROUP_ID", None)
        self.group_id = group_id
        self.logger.debug(f"group_id={self.group_id}")

        self._parse_private_allow_list(private_allow_list)

        self._conn = gitlab.Gitlab(
            private_token=private_token,
            user_agent=f"gitlab-compliance-tool/{__version__}",
        )
        self.logger.debug("Finished initialization")

    def _validate(self):
        """
        Validates if we have what we need to run the command.
        If not, error and exit.

        :raises SystemExit: If validation fails
        """
        if self.group_id is None:
            self.logger.error(
                "Please provide a group id using -g/--group-id"
                " or GCT_GROUP_ID env variable"
            )
            raise SystemExit(1)

    def _parse_private_allow_list(self, allow_list: Optional[str] = None):
        """
        Parses and sets up the list containing allowed and expected
        private projects. The expected format for the variable is
        a comma separated string like so::

            project one, proj2, myproject, A Long Named Project

        :param allow_list: List of projects that are allowed to be private
        """
        self._private_allow_list = []
        if allow_list is None:
            self.logger.debug("Looking for GCT_PRIVATE_ALLOW_LIST")
            allow_list = os.getenv("GCT_PRIVATE_ALLOW_LIST")
            # If we still have None as the allow_list then return early
            if allow_list is None:
                self.logger.debug("No allow list found")
                return

        self.logger.debug(f"Found allow list: {allow_list}")
        for item in allow_list.split(","):
            self._private_allow_list.append(item.strip())

    def check(self):
        """
        Checks the status of all projects under a group hirearchy.

        .. todo::
           Break out each check into it's own method which gets called
           from `check()`.
        """
        self._validate()
        top_level = self._conn.groups.get(self.group_id)
        for group in top_level.subgroups.list(iterator=True):
            for subgroup in self._list_subgroups(group):
                self.logger.debug(f"Found {subgroup.name} under {group.name}")
                for project in subgroup.projects.list(iterator=True):
                    self._prj_cnt += 1
                    self.logger.debug(
                        f"Found {project.name} under {subgroup.name}"
                    )

                    # Shortcut due to the size of the string
                    name = f"{group.name}->{subgroup.name}->{project.name}"
                    found_license = self._find_license(project.id)

                    if found_license is None:
                        self.logger.warning(
                            f"{name} is not licensed. See {project.web_url}"
                        )

                    if project.visibility != "public":
                        if str(project.id) not in self._private_allow_list:
                            self.logger.warning(
                                f"{name} is not public: {project.visibility}."
                                f" See {project.web_url}"
                            )
                        else:
                            self.logger.debug(
                                f"{project.name} is allowed to be private"
                            )
        self.logger.info(f"{self._grp_cnt} groups found")
        self.logger.info(f"{self._prj_cnt} projects processed")

    def _list_subgroups(
        self, group: gitlab.v4.objects.Group
    ) -> gitlab.v4.objects.groups.Group:
        """
        Recursive finder of subgroups.

        :param group: GitLab group to search under for subgroups
        """
        tmp_top_level = self._conn.groups.get(group.id)
        self.logger.debug(f"Recursing down {tmp_top_level.name}")
        for subgroup in tmp_top_level.subgroups.list(iterator=True):
            self.logger.debug(f"Returning leaf: {subgroup.name}")
            self._grp_cnt += 1
            yield from self._list_subgroups(subgroup)
        yield tmp_top_level

    def _find_license(self, project_id: int) -> Optional[str]:
        """
        Attempts to find the license information through a few methods.

        :param project_id: GitLab project id to look up for a license check
        """
        # Get license information from gitlab. We use a direct REST call
        # as the gitlab library doesn't show support for getting license info
        resp = requests.get(
            f"https://gitlab.com/api/v4/projects/{project_id}?license=true"
        )
        data = resp.json()
        if "license" in data.keys() and data["license"] is not None:
            self.logger.debug("Found license via gitlab")
            return resp.json()["license"]["name"]

        try:
            # look for reuse https://reuse.software/
            project = self._conn.projects.get(project_id)
            for file in project.repository_tree(iterator=True):
                if file["name"] == ".reuse" and file["type"] == "tree":
                    self.logger.debug("Found license via reuse directory")
                    return "(reuse compliant: see .reuse/)"
        except gitlab.exceptions.GitlabGetError as error:
            # This happens if a repository is empty
            self.logger.debug(f"Possible empty repository. Error: {error}")

        return None


def main() -> None:
    """
    Main function for the tool in charge of getting command line
    arguments from the invoker to the class instance.

    .. warning::
       `--private-allow-list` may be deprecated in favor of another
       pattern for skipping projects within a namespace.
    """
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-p",
        "--private-token",
        help="Private token (env var: GCT_PRIVATE_TOKEN)",
    )
    parser.add_argument(
        "-g", "--group-id", help="Group ID (env var: GCT_GROUP_ID)"
    )
    parser.add_argument(
        "-l",
        "--log-level",
        default="info",
        choices=("debug", "info", "warning", "error", "critical"),
        help="Log level for output: debug, info, warning, error, critical",
    )
    parser.add_argument(
        "-a",
        "--private-allow-list",
        help=(
            "Comma separated string of project id's which are allowed"
            " to be private. (env var: GCT_PRIVATE_ALLOW_LIST). PLEASE NOTE:"
            " this interface may be deprecated in favor of another pattern"
            " for skipping projects within a namespace"
        ),
    )

    args = parser.parse_args()

    # Set up our logger
    logger = logging.getLogger("gitlab-compliance-tool")
    logger.setLevel(args.log_level.upper())
    handler = logging.StreamHandler()
    format_tpl = ["%(asctime)s", "%(levelname)s", "%(message)s"]
    # If we are in debug, add in more info
    if logger.level == 10:
        format_tpl.insert(1, "%(filename)s:%(funcName)s():%(lineno)d")

    # Set the formatter by joining the template with ' - ' between
    handler.setFormatter(logging.Formatter(" - ".join(format_tpl)))
    logger.handlers.append(handler)

    c = GitLabComplianceTool(
        logger,
        private_token=args.private_token,
        group_id=args.group_id,
        private_allow_list=args.private_allow_list,
    )
    try:
        c.check()
    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    main()
